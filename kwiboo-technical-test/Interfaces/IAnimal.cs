﻿namespace kwiboo_technical_test.Interfaces
{
    public interface IAnimal
    {
        public string Name { get; }

        string GetSound();
    }
}
