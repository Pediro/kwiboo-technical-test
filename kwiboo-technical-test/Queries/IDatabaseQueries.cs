﻿using kwiboo_technical_test.Interfaces;
using kwiboo_technical_test.Models;

namespace kwiboo_technical_test.Queries
{
    public interface IDatabaseQueries
    {
        List<AnimalResultModel> GetAnimals();
        List<AnimalResultModel> GetMammals();
        AnimalResultModel? GetAnimalByName(string name);
        string GetSerialisedData();
        string CompareTypes();
    }
}
