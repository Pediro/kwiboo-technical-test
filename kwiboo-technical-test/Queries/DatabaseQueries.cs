﻿using kwiboo_technical_test.Data;
using kwiboo_technical_test.Interfaces;
using kwiboo_technical_test.Models;
using kwiboo_technical_test.Services;
using System.Text.Json;
using System.Xml.Linq;

namespace kwiboo_technical_test.Queries
{
    public class DatabaseQueries : IDatabaseQueries
    {
        private DataStore _db = new DataStore();
        private readonly ILoggingService _loggingService;

        public DatabaseQueries(ILoggingService loggingService)
        {
            _loggingService = loggingService;
        }

        public List<AnimalResultModel> GetAnimals()
        {
            try 
            {
                return _db.AnimalDatabase.Select(a => new AnimalResultModel
                {
                    Name = a.Name,
                    Sound = a.GetSound()
                }).ToList();
            } catch (Exception ex)
            {
                _loggingService.LogError(ex);
                return new List<AnimalResultModel>();
            }
        }

        public List<AnimalResultModel> GetMammals()
        {
            try
            {
                return _db.AnimalDatabase.OfType<IMammal>().Select(a => new AnimalResultModel
                {
                    Name = a.Name,
                    Sound = a.GetSound()
                }).ToList();
            }
            catch (Exception ex)
            {
                _loggingService.LogError(ex);
                return new List<AnimalResultModel>();
            }
        }

        public AnimalResultModel? GetAnimalByName(string name)
        {
            try
            {
                return _db.AnimalDatabase.Where(a => a.Name.ToLower() == name.ToLower()).Select(a => new AnimalResultModel
                {
                    Name = a.Name,
                    Sound = a.GetSound()
                }).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _loggingService.LogError(ex);
                return null;
            }
        }

        public string GetSerialisedData()
        {
            try
            {
                return JsonSerializer.Serialize(GetAnimals());
            }
            catch (Exception ex)
            {
                _loggingService.LogError(ex);
                return "";
            }
        }

        public string CompareTypes()
        {
            try
            {
                string comparisonResults = "";

                foreach (IAnimal animal in _db.AnimalDatabase)
                {
                    if (animal is Cat)
                        comparisonResults += $"{animal.Name} is a cat. ";
                    else if (animal is Dog)
                        comparisonResults += $"{animal.Name} is a dog. ";
                    else if (animal is Bird)
                        comparisonResults += $"{animal.Name} is a bird. ";
                    else
                        comparisonResults += $"Unsure what the animal {animal.Name} is.";
                }

                return comparisonResults;
            }
            catch (Exception ex)
            {
                _loggingService.LogError(ex);
                return "";
            }
        }
    }
}
