﻿let resultContainer = document.querySelector('[data-result]');

function clearResults() {
    resultContainer.textContent = '';
}

function sendGetRequest(requestUrl, callback) {
    clearResults();

    const request = new XMLHttpRequest();

    request.onreadystatechange = function () {
        if (request.readyState === XMLHttpRequest.DONE) {
            if (request.status === 200) {
                callback(request.responseText);
            }
            else if (request.status === 204) {
                resultContainer.textContent = "No results."
            }
            else {
                resultContainer.textContent = "An error occurred."
            }
        }
    };

    request.open('Get', requestUrl);
    request.send();
}

let getAllAnimalsBtn = document.querySelector('[data-btn-get-all-animals]');
getAllAnimalsBtn.addEventListener('click', function () {

    sendGetRequest('api/getallanimals', function (responseJsonString) {

        let responseJson = JSON.parse(responseJsonString);

        if (responseJson === null) {
            resultContainer.textContent = "No results."
        } else {
            responseJson.forEach(function (item) {
                let dataRow = document.createElement('div');

                dataRow.textContent = `Name: ${item.name}, Sound: ${item.sound}`;

                resultContainer.append(dataRow);
            });
        }
    });
});

let getSeralisedDb = document.querySelector('[data-btn-get-seralised-db]');
getSeralisedDb.addEventListener('click', function () {
    sendGetRequest('api/GetSerialisedData', function (responseJsonString) {

        if (responseJsonString === null) {
            resultContainer.textContent = "No results."
        } else {
            resultContainer.textContent = responseJsonString;
        }
    });
});

let getMammalsBtn = document.querySelector('[data-btn-get-mammals]');
getMammalsBtn.addEventListener('click', function () {
    sendGetRequest('api/getmammals', function (responseJsonString) {

        let responseJson = JSON.parse(responseJsonString);

        if (responseJson === null) {
            resultContainer.textContent = "No results."
        } else {
            responseJson.forEach(function (item) {
                let dataRow = document.createElement('div');
                dataRow.textContent = `Name: ${item.name}, Sound: ${item.sound}`;

                resultContainer.append(dataRow);
            });
        }
    });
});

let getAnimalTypesBtn = document.querySelector('[data-btn-get-animal-types]');
getAnimalTypesBtn.addEventListener('click', function () {
    sendGetRequest('api/getanimaltypes', function (responseJsonString) {

        if (responseJsonString === null) {
            resultContainer.textContent = "No results."
        } else {
            resultContainer.textContent = responseJsonString;
        }
    });
})

let animalSearchInput = document.querySelector('[data-input-animal-name]');
let animalSearchBtn = document.querySelector('[data-btn-animal-name-search]');
animalSearchBtn.addEventListener('click', function () {

    if (animalSearchInput.value === '')
        return;

    sendGetRequest(`api/getanimalbyname?name=${animalSearchInput.value}`, function (responseJsonString) {

        let responseJson = JSON.parse(responseJsonString);

        if (responseJson === null) {
            resultContainer.textContent = "No results."
        } else {
            resultContainer.textContent = `Name: ${responseJson.name}, Sound: ${responseJson.sound}`;
        }
    });
});