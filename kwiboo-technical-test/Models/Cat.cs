﻿using kwiboo_technical_test.Interfaces;

namespace kwiboo_technical_test.Models
{
    public class Cat : IMammal
    {
        public string Name { get => "Cat"; }

        public string GetSound()
        {
            return "Meow";
        }
    }
}
