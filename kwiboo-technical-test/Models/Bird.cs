﻿using kwiboo_technical_test.Interfaces;

namespace kwiboo_technical_test.Models
{
    public class Bird : IAnimal
    {
        public string Name { get => "Bird"; }

        public string GetSound()
        {
            return "Chirp";
        }
    }
}
