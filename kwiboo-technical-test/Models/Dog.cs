﻿using kwiboo_technical_test.Interfaces;

namespace kwiboo_technical_test.Models
{
    public class Dog : IMammal
    {
        public string Name { get => "Dog"; }

        public string GetSound()
        {
            return "Bark";
        }
    }
}