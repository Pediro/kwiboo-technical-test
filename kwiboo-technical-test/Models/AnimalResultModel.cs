﻿using Newtonsoft.Json;

namespace kwiboo_technical_test.Models
{
    public class AnimalResultModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("sound")]
        public string Sound { get; set; }
    }
}
