﻿using kwiboo_technical_test.Interfaces;
using kwiboo_technical_test.Models;
using kwiboo_technical_test.Queries;
using Microsoft.AspNetCore.Mvc;

namespace kwiboo_technical_test.Controllers
{
    public class ApiController : Controller
    {
        private readonly IDatabaseQueries _databaseQueries;
        
        public ApiController(IDatabaseQueries databaseQueries)
        {
            _databaseQueries = databaseQueries;
        }

        public IActionResult Index()
        {
            return View();
        }

        public ActionResult GetAllAnimals()
        {
            List<AnimalResultModel> animals = _databaseQueries.GetAnimals();

            return Ok(animals);
        }

        public ActionResult GetMammals()
        {
            List<AnimalResultModel> mammals = _databaseQueries.GetMammals();

            return Ok(mammals);
        }

        public ActionResult GetAnimalByName(string name)
        {
            AnimalResultModel? animal = _databaseQueries.GetAnimalByName(name);

            return Ok(animal);
        }

        public ActionResult GetSerialisedData()
        {
            string jsonString = _databaseQueries.GetSerialisedData();

            return Ok(jsonString);
        }

        public ActionResult GetAnimalTypes()
        {
            string result = _databaseQueries.CompareTypes();

            return Ok(result);
        }
    }
}
