﻿using kwiboo_technical_test.Interfaces;
using kwiboo_technical_test.Models;
using kwiboo_technical_test.Services;
using System.Text.Json;

namespace kwiboo_technical_test.Data
{
    public class DataStore
    {
        public List<IAnimal> AnimalDatabase = new List<IAnimal>
            {
                new Cat(),
                new Dog(),
                new Bird()
            };        
    }
}
