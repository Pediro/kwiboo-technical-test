﻿namespace kwiboo_technical_test.Services
{
    public class LoggingService : ILoggingService
    {
        public void LogError(Exception ex)
        {
            Console.WriteLine("Error:" + ex.ToString());
        }

        public void LogInformation(string message)
        {
            Console.WriteLine("Information:" + message);
        }
    }
}
