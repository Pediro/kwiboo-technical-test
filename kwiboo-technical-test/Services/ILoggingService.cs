﻿namespace kwiboo_technical_test.Services
{
    public interface ILoggingService
    {
        public void LogError(Exception ex);

        public void LogInformation(string message);
    }
}
